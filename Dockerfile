FROM bellsoft/liberica-openjre-debian:17
WORKDIR /app
COPY build/libs/*.jar app.jar

CMD java $JAVA_OPTS -jar app.jar
