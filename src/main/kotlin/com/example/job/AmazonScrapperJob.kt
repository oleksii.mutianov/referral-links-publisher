package com.example.job

import java.util.concurrent.TimeUnit
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class AmazonScrapperJob {

    @Scheduled(fixedDelay = 2, timeUnit = TimeUnit.SECONDS)
    fun scrap() {
        println("Scraping...")
    }

}