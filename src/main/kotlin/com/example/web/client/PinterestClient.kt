package com.example.web.client

import com.example.web.dto.PinResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable

@FeignClient(name = "pinterest", url = "https://api.pinterest.com/v1")
interface PinterestClient {

    @GetMapping("/pins/{pinId}/")
    fun getPin(@PathVariable pinId: String): PinResponse

}