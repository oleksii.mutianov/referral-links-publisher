package com.example.web.dto

data class PinResponse(
    val id: String,
    val link: String,
)
